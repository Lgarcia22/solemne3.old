/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.solemne3.services;

import com.mycompany.solemne3.dao.ComunasJpaController;
import com.mycompany.solemne3.entity.Comunas;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author luisg
 */
@Path("comunas")
public class ComunaRest {
    

@GET
@Produces(MediaType.APPLICATION_JSON)
public Response listarComunas(){
   
 ComunasJpaController dao=new ComunasJpaController ();  
 
    List<Comunas> comunas= dao.findComunasEntities();
    
   return Response.ok(200).entity(comunas).build();
    
}
}
